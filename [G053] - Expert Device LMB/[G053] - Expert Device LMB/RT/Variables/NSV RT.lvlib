﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="HMI to RT" Type="Folder">
		<Item Name="Auto Ops" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:ElemSize" Type="Str">1</Property>
			<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">1</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typedefName1" Type="Str">Data Struct - Tape Auto Data.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../../../Shared/Control/Data Struct - Tape Auto Data.ctl</Property>
			<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&amp;A1!!!"E!A!!!!!!$!!Z!)1F"&gt;82P)%^Q=T]!%U!+!!V5=GFH:W6S)%2X:7RM!%=!]1!!!!!!!!!")%2B&gt;'%A5X2S&gt;7.U)#UA6'&amp;Q:3""&gt;82P)%2B&gt;'%O9X2M!"Z!5!!#!!!!!1Z598"F)%&amp;V&gt;']A2'&amp;U91!!!1!#!!!!!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Close NS" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Direction Motor" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:ElemSize" Type="Str">1</Property>
			<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">1</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typedefName1" Type="Str">Direction Data.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../../../HMI/Shared/Controls/Direction Data.ctl</Property>
			<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!";6A!!!"E!A!!!!!!"!%1!]1!!!!!!!!!"%E2J=G6D&gt;'FP&lt;C"%982B,G.U&lt;!!J1"9!!A&gt;'&lt;X*X98*E"V*F&gt;G6S=W5!$5.P&lt;C"%;8*F9X2J&lt;WY!!1!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Edge Move" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network,Initial Value</Property>
			<Property Name="Initial Value:Value" Type="Str">False</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="Path" Type="Str">/ExpertDevice.lvproj/NI-sbRIO-9607-G053/RT/Variables/NSV RT.lvlib/HMI to RT/</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Joy Button 3" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Joystick" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:ElemSize" Type="Str">1</Property>
			<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">1</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typedefName1" Type="Str">Data Struct - Joystick Data.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../../../HMI/Shared/Controls/Data Struct - Joystick Data.ctl</Property>
			<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$3TA!!!"E!A!!!!!!(!!Z!)1F%;8*F9X2J&lt;WY!#U!+!!64='6F:!!31#%-27ZB9GRF)%VP&gt;'^S!!!31#%-5X2B=H1A47^U;7^O!!!)1#%$4%6%!"&gt;!#!!26'&amp;S:W6U)#-A&lt;W9A=(6M=W5!4A$R!!!!!!!!!!%@2'&amp;U93"4&gt;(*V9X1A,3"+&lt;XFT&gt;'FD;S"%982B,G.U&lt;!!G1&amp;!!"A!!!!%!!A!$!!1!"1Z+&lt;XFT&gt;'FD;S"%982B)!!!!1!'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Laser Data" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:ElemSize" Type="Str">1</Property>
			<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">1</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typedefName1" Type="Str">Dist Beam Str.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../../../HMI/Shared/Controls/Dist Beam Str.ctl</Property>
			<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&lt;FQ!!!"E!A!!!!!!%!".!#A!.1G6B&lt;3"4&gt;(*F&lt;G&gt;U;!!61!I!$ERB=W6S)%2J=X2B&lt;G.F!!!21!I!#EZF&gt;S"598*H:81!!$9!]1!!!!!!!!!"%52J=X1A1G6B&lt;3"4&gt;()O9X2M!"R!5!!$!!!!!1!##ERB=W6S)%2B&gt;'%!!!%!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Operation Mode" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:ElemSize" Type="Str">1</Property>
			<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">1</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typedefName1" Type="Str">Operation.ctl</Property>
			<Property Name="typedefName2" Type="Str">Enum - Operation Mode.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../../../HMI/Shared/Controls/Operation.ctl</Property>
			<Property Name="typedefPath2" Type="PathRel">../../../Shared/Control/Enum - Operation Mode.ctl</Property>
			<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"`?Q!!!"E!A!!!!!!#!$V!&amp;A!#%ERB=W6S)'&amp;T)&amp;*F:G6S:7ZD:2**&lt;7&amp;H:3"B=S"3:7:F=G6O9W5!$E^Q:8*B&gt;'FP&lt;C".&lt;W2F!!!M!0%!!!!!!!!!!1V0='6S982J&lt;WYO9X2M!":!5!!"!!!*4X"F=G&amp;U;7^O!!%!!1!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Paused Ops" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network,Initial Value</Property>
			<Property Name="Initial Value:Value" Type="Str">False</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="Path" Type="Str">/ExpertDevice.lvproj/NI-sbRIO-9607-G053/RT/Variables/NSV RT.lvlib/HMI to RT/</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Restart Cumm" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="ROI Settings" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:ElemSize" Type="Str">1</Property>
			<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">1</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typedefName1" Type="Str">ROI Parameters.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../../../HMI/Shared/Controls/ROI Parameters.ctl</Property>
			<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#GIA!!!"E!A!!!!!!'!!N!!Q!%4'6G&gt;!!!#5!$!!.5&lt;X!!#U!$!!63;7&gt;I&gt;!!.1!-!"E*P&gt;(2P&lt;1!!$U!+!!B3&lt;X2B&gt;'FP&lt;A!!0Q$R!!!!!!!!!!%35E^*)&amp;"B=G&amp;N:82F=H-O9X2M!#2!5!!&amp;!!!!!1!#!!-!"!Z34UEA5'&amp;S97VF&gt;'6S=Q!!!1!&amp;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Scale Interval" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!F)1!!!"E!A!!!!!!"!!V!"Q!'65FO&gt;$-S!!!"!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Start" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Stop Operation" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"E!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Target Edge" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Initial Value:Value" Type="Str">0.100000</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="Path" Type="Str">/ExpertDevice.lvproj/NI-sbRIO-9607-G053/RT/Variables/NSV RT.lvlib/HMI to RT/</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typedefName1" Type="Str">Target Edge.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../../../Shared/Control/Target Edge.ctl</Property>
			<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"E!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="Target Window" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Initial Value:Value" Type="Str">0.000000</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="Path" Type="Str">/ExpertDevice.lvproj/NI-sbRIO-9607-G053/RT/Variables/NSV RT.lvlib/HMI to RT/</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"E!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
	</Item>
	<Item Name="RT to HMI" Type="Folder">
		<Item Name="Edge Data" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:ElemSize" Type="Str">1</Property>
			<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">True</Property>
			<Property Name="numTypedefs" Type="UInt">1</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typedefName1" Type="Str">Edge Info.ctl</Property>
			<Property Name="typedefPath1" Type="PathRel">../../Controls/Edge Info.ctl</Property>
			<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$2T1!!!"E!A!!!!!!'!"F!#!!31X6S=G6O&gt;#"&amp;:'&gt;F)%.P&gt;7ZU!!!&lt;1!A!&amp;5.V&lt;86M982J&gt;G5A272H:3"$&lt;X6O&gt;!!&lt;1!E!&amp;%.V=H*F&lt;H1A1W6O&gt;(*F)&amp;*B&gt;'FP!!!71#%25G6B9WAA6'&amp;S:W6U)%6E:W5!$E!B#%.F&lt;H2S:71`!!!U!0%!!!!!!!!!!1V&amp;:'&gt;F)%FO:G]O9X2M!"Z!5!!&amp;!!!!!1!#!!-!"!F&amp;:'&gt;F)%2B&gt;'%!!1!&amp;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
	</Item>
</Library>
