﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="10008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="E1735A.DLL" Type="Document" URL="../E1735A.DLL"/>
		<Item Name="E1735ACore.DLL" Type="Document" URL="../E1735ACore.DLL"/>
		<Item Name="E1736A.DLL" Type="Document" URL="../E1736A.DLL"/>
		<Item Name="E1736ACore.DLL" Type="Document" URL="../E1736ACore.DLL"/>
		<Item Name="L5530A.ico" Type="Document" URL="../L5530A.ico"/>
		<Item Name="L5530A_TrigExample.vi" Type="VI" URL="../L5530A_TrigExample.vi"/>
		<Item Name="L5530dll.dll" Type="Document" URL="../L5530dll.dll"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="compatWriteText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatWriteText.vi"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Write File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write File+ (string).vi"/>
			</Item>
			<Item Name="ChkFileName.vi" Type="VI" URL="../MyTools/ChkFileName.vi"/>
			<Item Name="E1735A_ChangeTrigger.vi" Type="VI" URL="../MainFunctions/E1735A_ChangeTrigger.vi"/>
			<Item Name="E1735A_GetParameter.vi" Type="VI" URL="../MainFunctions/E1735A_GetParameter.vi"/>
			<Item Name="E1735A_ReadAllSamples.vi" Type="VI" URL="../MainFunctions/E1735A_ReadAllSamples.vi"/>
			<Item Name="E1735A_ReadBeamStrength.vi" Type="VI" URL="../MainFunctions/E1735A_ReadBeamStrength.vi"/>
			<Item Name="E1735A_ReadDeviceCount.vi" Type="VI" URL="../MainFunctions/E1735A_ReadDeviceCount.vi"/>
			<Item Name="E1735A_ReadSampleCount.vi" Type="VI" URL="../MainFunctions/E1735A_ReadSampleCount.vi"/>
			<Item Name="E1735A_Reset.vi" Type="VI" URL="../MainFunctions/E1735A_Reset.vi"/>
			<Item Name="E1735A_ResetDevice.vi" Type="VI" URL="../MainFunctions/E1735A_ResetDevice.vi"/>
			<Item Name="E1735A_SelectDevice.vi" Type="VI" URL="../MainFunctions/E1735A_SelectDevice.vi"/>
			<Item Name="E1735A_SetOptics.vi" Type="VI" URL="../MainFunctions/E1735A_SetOptics.vi"/>
			<Item Name="E1735A_SetParameter.vi" Type="VI" URL="../MainFunctions/E1735A_SetParameter.vi"/>
			<Item Name="E1735A_SetSampleTriggers.vi" Type="VI" URL="../MainFunctions/E1735A_SetSampleTriggers.vi"/>
			<Item Name="ErrorWindow.vi" Type="VI" URL="../MyTools/ErrorWindow.vi"/>
			<Item Name="FillWhiteSpace.vi" Type="VI" URL="../MyTools/FillWhiteSpace.vi"/>
			<Item Name="GetControlRefNum.vi" Type="VI" URL="../MyTools/GetControlRefNum.vi"/>
			<Item Name="GetProgramRootPath.vi" Type="VI" URL="../MyTools/GetProgramRootPath.vi"/>
			<Item Name="L5530A_dllError.vi" Type="VI" URL="../MyTools/L5530A_dllError.vi"/>
			<Item Name="L5530A_E1736A_FinalizeDLL.vi" Type="VI" URL="../MainFunctions/L5530A_E1736A_FinalizeDLL.vi"/>
			<Item Name="L5530A_E1736A_InitializeDLL.vi" Type="VI" URL="../MainFunctions/L5530A_E1736A_InitializeDLL.vi"/>
			<Item Name="L5530A_E1736A_ReadDeviceCount.vi" Type="VI" URL="../MainFunctions/L5530A_E1736A_ReadDeviceCount.vi"/>
			<Item Name="L5530A_E1736A_ReadEnvironment.vi" Type="VI" URL="../MainFunctions/L5530A_E1736A_ReadEnvironment.vi"/>
			<Item Name="L5530A_E1736A_SelectDevice.vi" Type="VI" URL="../MainFunctions/L5530A_E1736A_SelectDevice.vi"/>
			<Item Name="L5530A_EnterEnvironment.vi" Type="VI" URL="../MainFunctions/L5530A_EnterEnvironment.vi"/>
			<Item Name="L5530A_ErrorHandler.vi" Type="VI" URL="../MyTools/L5530A_ErrorHandler.vi"/>
			<Item Name="L5530A_LaserModelEnum.ctl" Type="VI" URL="../MainFunctions/L5530A_LaserModelEnum.ctl"/>
			<Item Name="L5530A_Log.vi" Type="VI" URL="../MyTools/L5530A_Log.vi"/>
			<Item Name="L5530A_ReadBeamStrength.vi" Type="VI" URL="../MainFunctions/L5530A_ReadBeamStrength.vi"/>
			<Item Name="L5530A_ReadEnvironment.vi" Type="VI" URL="../MainFunctions/L5530A_ReadEnvironment.vi"/>
			<Item Name="L5530A_SensorsCluster.ctl" Type="VI" URL="../MainFunctions/L5530A_SensorsCluster.ctl"/>
			<Item Name="L5530A_SetAllParameter.vi" Type="VI" URL="../MainFunctions/L5530A_SetAllParameter.vi"/>
			<Item Name="L5530A_StartCal.ctl" Type="VI" URL="../MainFunctions/L5530A_StartCal.ctl"/>
			<Item Name="L5530A_StartCal.vi" Type="VI" URL="../MainFunctions/L5530A_StartCal.vi"/>
			<Item Name="L5530A_StartComp.ctl" Type="VI" URL="../MainFunctions/L5530A_StartComp.ctl"/>
			<Item Name="L5530A_StartComp.vi" Type="VI" URL="../MainFunctions/L5530A_StartComp.vi"/>
			<Item Name="L5530A_StartLaserBeamStrength.vi" Type="VI" URL="../MainFunctions/L5530A_StartLaserBeamStrength.vi"/>
			<Item Name="L5530A_Stop.vi" Type="VI" URL="../MainFunctions/L5530A_Stop.vi"/>
			<Item Name="L5530A_StopLaserBeamStrength.vi" Type="VI" URL="../MainFunctions/L5530A_StopLaserBeamStrength.vi"/>
			<Item Name="L5530A_SystemCheck.ctl" Type="VI" URL="../MainFunctions/L5530A_SystemCheck.ctl"/>
			<Item Name="L5530A_SystemCheck.vi" Type="VI" URL="../MainFunctions/L5530A_SystemCheck.vi"/>
			<Item Name="L5530A_TempSensorsAverage.vi" Type="VI" URL="../MainFunctions/L5530A_TempSensorsAverage.vi"/>
			<Item Name="L5530A_TrigExample.ctl" Type="VI" URL="../L5530A_TrigExample.ctl"/>
			<Item Name="L5530A_UnitStandardEnum.ctl" Type="VI" URL="../MainFunctions/L5530A_UnitStandardEnum.ctl"/>
			<Item Name="L5530A_UpdateEnvironment.vi" Type="VI" URL="../MainFunctions/L5530A_UpdateEnvironment.vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/C/Program Files/National Instruments/LabVIEW 2010/resource/lvanlys.dll"/>
			<Item Name="OpticsEnum.ctl" Type="VI" URL="../MainFunctions/OpticsEnum.ctl"/>
			<Item Name="ParameterEnum.ctl" Type="VI" URL="../MainFunctions/ParameterEnum.ctl"/>
			<Item Name="SideCluster.ctl" Type="VI" URL="../MyTools/SideCluster.ctl"/>
			<Item Name="TLaserSample.ctl" Type="VI" URL="../MainFunctions/TLaserSample.ctl"/>
			<Item Name="TriggersRing.ctl" Type="VI" URL="../MainFunctions/TriggersRing.ctl"/>
			<Item Name="WriteFile.vi" Type="VI" URL="../MyTools/WriteFile.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="L5530A_TrigExample" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{CDD3EA66-7457-4CB4-942E-183A75DCE808}</Property>
				<Property Name="App_INI_GUID" Type="Str">{8B2B2425-3654-4C19-87E4-344E66F3F8B3}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">L5530A_TrigExample</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/L5530A_TrigExample/L5530A_TrigExample</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Destination[0].destName" Type="Str">L5530A_TrigExample.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/L5530A_TrigExample/L5530A_TrigExample.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/L5530A_TrigExample</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/L5530A.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{AC95792B-5B7B-42FD-9A94-B1A2EC6A311D}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/L5530A_TrigExample.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/E1735A.DLL</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/E1735ACore.DLL</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/E1736A.DLL</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/E1736ACore.DLL</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/L5530dll.dll</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">7</Property>
				<Property Name="TgtF_companyName" Type="Str">Calame Métrologie SA</Property>
				<Property Name="TgtF_fileDescription" Type="Str">L5530A_TrigExample</Property>
				<Property Name="TgtF_fileVersion.major" Type="Int">1</Property>
				<Property Name="TgtF_internalName" Type="Str">L5530A_TrigExample</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2011 Calame Métrologie SA</Property>
				<Property Name="TgtF_productName" Type="Str">L5530A_TrigExample</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{8D7D5E48-F4B5-48F9-9CC3-49646308494D}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">L5530A_TrigExample.exe</Property>
			</Item>
			<Item Name="L5530A_TrigExample-Distribution" Type="Source Distribution">
				<Property Name="Bld_buildSpecName" Type="Str">L5530A_TrigExample-Distribution</Property>
				<Property Name="Bld_excludedDirectory[0]" Type="Path">vi.lib</Property>
				<Property Name="Bld_excludedDirectory[0].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[1]" Type="Path">resource/objmgr</Property>
				<Property Name="Bld_excludedDirectory[1].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[2]" Type="Path">/C/Documents and Settings/dar0n/Mes documents/LabVIEW Data/InstCache</Property>
				<Property Name="Bld_excludedDirectory[3]" Type="Path">instr.lib</Property>
				<Property Name="Bld_excludedDirectory[3].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectoryCount" Type="Int">4</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/L5530A_TrigExample/L5530A_TrigExample-Distribution</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Destination[0].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/L5530A_TrigExample-Distribution</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/L5530A_TrigExample-Distribution</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{96D2F927-8AD9-4000-A580-473EE64841BB}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/L5530A_TrigExample.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/E1735A.DLL</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/E1735ACore.DLL</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/E1736A.DLL</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/E1736ACore.DLL</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/L5530dll.dll</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">7</Property>
			</Item>
			<Item Name="L5530A_TrigExample-Installer" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">L5530A_TrigExample</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{C10277BE-A7DE-407A-81E6-2069DB3F4FCE}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="INST_author" Type="Str">Calame Métrologie SA</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">../builds/L5530A_TrigExample/L5530A_TrigExample-Installer</Property>
				<Property Name="INST_buildLocation.type" Type="Str">relativeToCommon</Property>
				<Property Name="INST_buildSpecName" Type="Str">L5530A_TrigExample-Installer</Property>
				<Property Name="INST_defaultDir" Type="Str">{C10277BE-A7DE-407A-81E6-2069DB3F4FCE}</Property>
				<Property Name="INST_productName" Type="Str">L5530A_TrigExample</Property>
				<Property Name="INST_productVersion" Type="Str">1.0.7</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="MSI_arpCompany" Type="Str">Calame Métrologie SA</Property>
				<Property Name="MSI_distID" Type="Str">{D5DE1859-6274-45AC-94CB-814AA9785AD0}</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{2D57C172-D031-48CC-B173-5976E3A2D33A}</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{C10277BE-A7DE-407A-81E6-2069DB3F4FCE}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{C10277BE-A7DE-407A-81E6-2069DB3F4FCE}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">L5530A_TrigExample.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">L5530A_TrigExample</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">Calame Métrologie</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{8D7D5E48-F4B5-48F9-9CC3-49646308494D}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">L5530A_TrigExample</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/L5530A_TrigExample</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
		</Item>
	</Item>
</Project>
