﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="E1735A.DLL" Type="Document" URL="../E1735A.DLL"/>
		<Item Name="E1735ACore.DLL" Type="Document" URL="../E1735ACore.DLL"/>
		<Item Name="E1736A.DLL" Type="Document" URL="../E1736A.DLL"/>
		<Item Name="E1736ACore.DLL" Type="Document" URL="../E1736ACore.DLL"/>
		<Item Name="L5530A.ico" Type="Document" URL="../L5530A.ico"/>
		<Item Name="L5530A_Example.vi" Type="VI" URL="../L5530A_Example.vi"/>
		<Item Name="L5530dll.dll" Type="Document" URL="../L5530dll.dll"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="compatWriteText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatWriteText.vi"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Write File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write File+ (string).vi"/>
			</Item>
			<Item Name="ChkFileName.vi" Type="VI" URL="../MyTools/ChkFileName.vi"/>
			<Item Name="E1735A_GetDLLRevision.vi" Type="VI" URL="../MainFunctions/E1735A_GetDLLRevision.vi"/>
			<Item Name="E1735A_GetParameter.vi" Type="VI" URL="../MainFunctions/E1735A_GetParameter.vi"/>
			<Item Name="E1735A_Measure.vi" Type="VI" URL="../MainFunctions/E1735A_Measure.vi"/>
			<Item Name="E1735A_ReadBeamStrength.vi" Type="VI" URL="../MainFunctions/E1735A_ReadBeamStrength.vi"/>
			<Item Name="E1735A_ReadDeviceCount.vi" Type="VI" URL="../MainFunctions/E1735A_ReadDeviceCount.vi"/>
			<Item Name="E1735A_ReadLastError.vi" Type="VI" URL="../MainFunctions/E1735A_ReadLastError.vi"/>
			<Item Name="E1735A_ReadSample.vi" Type="VI" URL="../MainFunctions/E1735A_ReadSample.vi"/>
			<Item Name="E1735A_Reset.vi" Type="VI" URL="../MainFunctions/E1735A_Reset.vi"/>
			<Item Name="E1735A_ResetDevice.vi" Type="VI" URL="../MainFunctions/E1735A_ResetDevice.vi"/>
			<Item Name="E1735A_SelectDevice.vi" Type="VI" URL="../MainFunctions/E1735A_SelectDevice.vi"/>
			<Item Name="E1735A_SetOptics.vi" Type="VI" URL="../MainFunctions/E1735A_SetOptics.vi"/>
			<Item Name="E1735A_SetParameter.vi" Type="VI" URL="../MainFunctions/E1735A_SetParameter.vi"/>
			<Item Name="ErrorWindow.vi" Type="VI" URL="../MyTools/ErrorWindow.vi"/>
			<Item Name="FillWhiteSpace.vi" Type="VI" URL="../MyTools/FillWhiteSpace.vi"/>
			<Item Name="GetControlRefNum.vi" Type="VI" URL="../MyTools/GetControlRefNum.vi"/>
			<Item Name="GetProgramRootPath.vi" Type="VI" URL="../MyTools/GetProgramRootPath.vi"/>
			<Item Name="L5530A_dllError.vi" Type="VI" URL="../MyTools/L5530A_dllError.vi"/>
			<Item Name="L5530A_E1736A_FinalizeDLL.vi" Type="VI" URL="../MainFunctions/L5530A_E1736A_FinalizeDLL.vi"/>
			<Item Name="L5530A_E1736A_InitializeDLL.vi" Type="VI" URL="../MainFunctions/L5530A_E1736A_InitializeDLL.vi"/>
			<Item Name="L5530A_E1736A_ReadDeviceCount.vi" Type="VI" URL="../MainFunctions/L5530A_E1736A_ReadDeviceCount.vi"/>
			<Item Name="L5530A_E1736A_ReadEnvironment.vi" Type="VI" URL="../MainFunctions/L5530A_E1736A_ReadEnvironment.vi"/>
			<Item Name="L5530A_E1736A_SelectDevice.vi" Type="VI" URL="../MainFunctions/L5530A_E1736A_SelectDevice.vi"/>
			<Item Name="L5530A_EnterEnvironment.vi" Type="VI" URL="../MainFunctions/L5530A_EnterEnvironment.vi"/>
			<Item Name="L5530A_ErrorHandler.vi" Type="VI" URL="../MyTools/L5530A_ErrorHandler.vi"/>
			<Item Name="L5530A_Example.ctl" Type="VI" URL="../L5530A_Example.ctl"/>
			<Item Name="L5530A_LaserModelEnum.ctl" Type="VI" URL="../MainFunctions/L5530A_LaserModelEnum.ctl"/>
			<Item Name="L5530A_Log.vi" Type="VI" URL="../MyTools/L5530A_Log.vi"/>
			<Item Name="L5530A_ReadBeamStrength.vi" Type="VI" URL="../MainFunctions/L5530A_ReadBeamStrength.vi"/>
			<Item Name="L5530A_ReadEnvironment.vi" Type="VI" URL="../MainFunctions/L5530A_ReadEnvironment.vi"/>
			<Item Name="L5530A_SetAllParameter.vi" Type="VI" URL="../MainFunctions/L5530A_SetAllParameter.vi"/>
			<Item Name="L5530A_StartCal.ctl" Type="VI" URL="../MainFunctions/L5530A_StartCal.ctl"/>
			<Item Name="L5530A_StartCal.vi" Type="VI" URL="../MainFunctions/L5530A_StartCal.vi"/>
			<Item Name="L5530A_StartComp.ctl" Type="VI" URL="../MainFunctions/L5530A_StartComp.ctl"/>
			<Item Name="L5530A_StartComp.vi" Type="VI" URL="../MainFunctions/L5530A_StartComp.vi"/>
			<Item Name="L5530A_StartLaserBeamStrength.vi" Type="VI" URL="../MainFunctions/L5530A_StartLaserBeamStrength.vi"/>
			<Item Name="L5530A_Stop.vi" Type="VI" URL="../MainFunctions/L5530A_Stop.vi"/>
			<Item Name="L5530A_StopLaserBeamStrength.vi" Type="VI" URL="../MainFunctions/L5530A_StopLaserBeamStrength.vi"/>
			<Item Name="L5530A_SystemCheck.ctl" Type="VI" URL="../MainFunctions/L5530A_SystemCheck.ctl"/>
			<Item Name="L5530A_SystemCheck.vi" Type="VI" URL="../MainFunctions/L5530A_SystemCheck.vi"/>
			<Item Name="L5530A_TempSensorsAverage.vi" Type="VI" URL="../MainFunctions/L5530A_TempSensorsAverage.vi"/>
			<Item Name="L5530A_UnitStandardEnum.ctl" Type="VI" URL="../MainFunctions/L5530A_UnitStandardEnum.ctl"/>
			<Item Name="L5530A_UpdateEnvironment.vi" Type="VI" URL="../MainFunctions/L5530A_UpdateEnvironment.vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="OpticsEnum.ctl" Type="VI" URL="../MainFunctions/OpticsEnum.ctl"/>
			<Item Name="ParameterEnum.ctl" Type="VI" URL="../MainFunctions/ParameterEnum.ctl"/>
			<Item Name="SensorsCluster.ctl" Type="VI" URL="../MainFunctions/SensorsCluster.ctl"/>
			<Item Name="SideCluster.ctl" Type="VI" URL="../MyTools/SideCluster.ctl"/>
			<Item Name="WriteFile.vi" Type="VI" URL="../MyTools/WriteFile.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="L5530A_Example" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{187B8A6C-C90C-4195-8ACB-C15FC850F34A}</Property>
				<Property Name="App_INI_GUID" Type="Str">{22EB9368-7FB7-4C45-B277-67D5FE9D38BB}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{B349D570-6F12-41F5-ABE5-5CE7E6A6D11E}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">L5530A_Example</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/L5530A_Example</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{B7A2EF60-B7C5-4AAF-96CC-52B3AB2F02CA}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">L5530A_Example.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/L5530A_Example/L5530A_Example.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/L5530A_Example</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/L5530A.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{4487DFC4-323F-4E78-B64E-D8FE42F56CB8}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/L5530A_Example.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/E1735A.DLL</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/E1735ACore.DLL</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/E1736A.DLL</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/E1736ACore.DLL</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/L5530dll.dll</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">7</Property>
				<Property Name="TgtF_companyName" Type="Str">Calame Métrologie SA</Property>
				<Property Name="TgtF_fileDescription" Type="Str">L5530A_Example</Property>
				<Property Name="TgtF_internalName" Type="Str">L5530A_Example</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2011 Calame Métrologie SA</Property>
				<Property Name="TgtF_productName" Type="Str">L5530A_Example</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{2D44BE85-82FD-4C22-BEBA-F380BF4B6AED}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">L5530A_Example.exe</Property>
			</Item>
			<Item Name="L5530A_Example-Distribution" Type="Source Distribution">
				<Property Name="Bld_buildCacheID" Type="Str">{F762CD6D-DFC4-4490-BB75-0375E1359A41}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">L5530A_Example-Distribution</Property>
				<Property Name="Bld_excludedDirectory[0]" Type="Path">vi.lib</Property>
				<Property Name="Bld_excludedDirectory[0].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[1]" Type="Path">resource/objmgr</Property>
				<Property Name="Bld_excludedDirectory[1].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[2]" Type="Path">/C/Documents and Settings/dar0n/Mes documents/LabVIEW Data/InstCache</Property>
				<Property Name="Bld_excludedDirectory[3]" Type="Path">instr.lib</Property>
				<Property Name="Bld_excludedDirectory[3].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectoryCount" Type="Int">4</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/L5530A_Example-Distribution</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{CDF26B49-F388-4782-AC21-0C61D12AA356}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/L5530A_Example-Distribution</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/L5530A_Example-Distribution</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{BC1A879A-20B2-42A3-8259-EA9C00DAE51C}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/L5530A_Example.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/E1735A.DLL</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/E1735ACore.DLL</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/E1736A.DLL</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/E1736ACore.DLL</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/L5530dll.dll</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">7</Property>
			</Item>
			<Item Name="L5530A_Example-Installer" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">L5530A_Example</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{39C8658F-CE68-4A3B-9631-3EA672A47666}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="INST_author" Type="Str">Calame Métrologie SA</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">../builds/L5530A_Example/L5530A_Example-Installer</Property>
				<Property Name="INST_buildLocation.type" Type="Str">relativeToCommon</Property>
				<Property Name="INST_buildSpecName" Type="Str">L5530A_Example-Installer</Property>
				<Property Name="INST_defaultDir" Type="Str">{39C8658F-CE68-4A3B-9631-3EA672A47666}</Property>
				<Property Name="INST_productName" Type="Str">L5530A_Example</Property>
				<Property Name="INST_productVersion" Type="Str">1.0.12</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">17008011</Property>
				<Property Name="MSI_arpCompany" Type="Str">Calame Métrologie SA</Property>
				<Property Name="MSI_distID" Type="Str">{F5808C36-7AD7-4EBA-BBFD-DC461B6600BC}</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{B062819E-DA85-4EA3-8503-538046F14277}</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{39C8658F-CE68-4A3B-9631-3EA672A47666}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{39C8658F-CE68-4A3B-9631-3EA672A47666}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">L5530A_Example.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">L5530A_Example</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">Calame Métrologie</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{2D44BE85-82FD-4C22-BEBA-F380BF4B6AED}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">L5530A_Example</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/L5530A_Example</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
		</Item>
	</Item>
</Project>
